###### PGMDASH

## Install dependencies

`npm install`

## Local Deployment

In the project directory, you can run:

### `npm run dev`

## To test charts

Use json file call 'sampleone.json' (../json-examples) -> **EXAMPLE NOT WORKING ANYMORE** - If you want to test it eitherway, you can use the component 'LineChart' or 'SeconChart' inside app.jsx

## UPDATE 21/02/2023

+ Now we're able to work with pgmetrics JSON file as an input. ('PgCharts.jsx' component)
+ We have 2 code examples for PgCharts component. 

    - One for a single chart (using 'indexes' key and 'size' value) 
    - and the other one for multiples charts using same examples from JSON File (such as, 'databases' and 'tables'). 
        
    - Read '..src/PgCharts.jsx' for more info.
