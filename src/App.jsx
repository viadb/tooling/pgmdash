import styles from "./style";
import { Footer, Navbar, Hero, Charts, LineCharts, SecondCharts, PgCharts } from "./components";
import Chart from "react-apexcharts";
import React, { Component } from "react";

const App = () => (
  <div className="w-full overflow-hidden">
    <div className={`${styles.paddingX} ${styles.flexCenter}`}>
      <div className={`${styles.boxWidth}`}>
        <Navbar />
      </div>
    </div>

    <div className={`${styles.flexStart}`}>
      <div className={`${styles.boxWidth}`}>
        <Hero />
      </div> 
    </div>
    
    <div className={` ${styles.paddingX} ${styles.flexCenter}`}>
      <div className={`${styles.boxWidth}`}>

        {/* <SecondCharts /> */}

        <PgCharts />

        {/* <Charts />

        <LineCharts />

        <Footer /> */}

      </div>
    </div>
  </div>
);

export default App;