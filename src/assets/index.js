import logo from "./logo.svg";
import facebook from "./facebook.svg";
import instagram from "./instagram.svg";
import linkedin from "./linkedin.svg";
import twitter from "./twitter.svg";
import menu from "./menu.svg";
import close from "./close.svg";

export {
  logo,
  facebook,
  instagram,
  linkedin,
  twitter,
  menu,
  close,
};
