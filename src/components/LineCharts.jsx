import React, { useState } from "react";
import ReactApexChart from "react-apexcharts";

const LineCharts = () => {
  const [data, setData] = useState([]);
  const [options, setOptions] = useState({
    chart: {
      type: "line"
    },
    xaxis: {
      categories: []
    }
  });

  const handleFileUpload = async e => {
    const file = e.target.files[0];
    const reader = new FileReader();
    reader.readAsText(file);
    reader.onload = () => {
      const fileData = JSON.parse(reader.result);
      setData(fileData);
      setOptions({
        ...options,
        xaxis: {
          ...options.xaxis,
          categories: fileData.map(d => d.label)
        }
      });
    };
  };

  return (
    <>
      <h1 className="font-poppins font-semibold text-black ss:text-[38px] text-[42px]"> Line Chart - Test it Here! </h1>
      <br />
      <input type="file" onChange={handleFileUpload} />
      <br />
      <ReactApexChart options={options} series={[{ name: "Data", data: data.map(d => d.value) }]} height={350} />
    </>
  );
};

export default LineCharts;