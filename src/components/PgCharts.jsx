// 1 - ONE CHART RENDERING EXAMPLE (IT WORKS)
// 2 - MULTIPLES CHARTS RENDERING EXAMPLE (PARTIALLY WORKS)


// 1 ------------------------- FUNCTIONAL CODE FOR ONE CHART AS EXAMPLE 
// THIS CODE WORKS WITH ONE KEY AS EXAMPLE, USING 'INDEXES' KEY

import React, { useState } from 'react';
import ReactApexChart from 'react-apexcharts';

const pgCharts = () => {
  const [data, setData] = useState(null);

  const handleFileUpload = (event) => {
    const reader = new FileReader();
    reader.onload = (event) => {
      const fileContent = event.target.result;
      const jsonData = JSON.parse(fileContent);
      setData(jsonData);
    };
    reader.readAsText(event.target.files[0]);
  };

  const chartOptions = {
    chart: {
      type: 'line',
    },
    series: [],
    xaxis: {
      categories: [],
    },
  };

  if (data) {
    const seriesData = [];
    const categoriesData = [];
    data.indexes.forEach((item) => {
      categoriesData.push(item.db_name);
      seriesData.push(item.size);
    });
    chartOptions.series = [{ data: seriesData, name: 'size' }];
    chartOptions.xaxis.categories = categoriesData;
  }

  return (
    <div>
      <div className='flex justify-center items-center'>
        <input type="file" className={`py-4 px-6 font-poppins font-medium text-[18px] text-primary bg-blue rounded-[10px] outline-none`} onChange={handleFileUpload} />
      </div>
      {data && (
        <ReactApexChart options={chartOptions} series={chartOptions.series} type="line" height={400} />
      )}
    </div>
  );
};

export default pgCharts;



// 2 ------------------------- TESTING THIS NEW CODE FOR MULTIPLE CHARTS
// USING 'INDEXES', 'TABLES' AND 'DATABASES' KEY AS EXAMPLE


// import React, { useState } from 'react';
// import ReactApexChart from 'react-apexcharts';

// const pgCharts = () => {
//   const [data, setData] = useState(null);

//   const handleFileUpload = (event) => {
//     const reader = new FileReader();
//     reader.onload = (event) => {
//       const fileContent = event.target.result;
//       const jsonData = JSON.parse(fileContent);
//       setData(jsonData);
//     };
//     reader.readAsText(event.target.files[0]);
//   };

//   const chartData = [
//     {
//       key: 'indexes',
//       type: 'bar',
//       nameKey: 'db_name',
//       valueKey: 'size',
//     },
//     {
//       key: 'tables',
//       type: 'line',
//       nameKey: 'db_name',
//       valueKey: 'size',
//     },
//     {
//       key: 'databases',
//       type: 'line',
//       nameKey: 'name',
//       valueKey: 'stats_reset',
//     },
//   ];

//   const chartOptions = {
//     chart: {
//       type: '',
//     },
//     series: [],
//     xaxis: {
//       categories: [],
//     },
//   };

//   const getIndexesChartData = () => {
//     const seriesData = [];
//     const categoriesData = [];
//     data.indexes.forEach((item) => {
//       categoriesData.push(item[chartData[0].nameKey]);
//       seriesData.push(item[chartData[0].valueKey]);
//     });
//     return { seriesData, categoriesData };
//   };

//   const getTablesChartData = () => {
//     const seriesData = [];
//     const categoriesData = [];
//     data.tables.forEach((item) => {
//       categoriesData.push(item[chartData[1].nameKey]);
//       seriesData.push(item[chartData[1].valueKey]);
//     });
//     return { seriesData, categoriesData };
//   };

//   const getDatabasesChartData = () => {
//     const seriesData = [];
//     const categoriesData = [];
//     data.databases.forEach((item) => {
//       categoriesData.push(item[chartData[2].nameKey]);
//       seriesData.push(item[chartData[2].valueKey]);
//     });
//     return { seriesData, categoriesData };
//   };

//   const renderCharts = () => {
//     return chartData.map((chart, index) => {
//       let chartDataFunc = null;
//       switch (chart.key) {
//         case 'tables':
//           chartDataFunc = getTablesChartData;
//           break;

//         case 'indexes':
//           chartDataFunc = getIndexesChartData;
//           break;

//         case 'databases':
//           chartDataFunc = getDatabasesChartData;
//           break;

//         default:
//           chartDataFunc = null;
//       }
//       const { seriesData, categoriesData } = chartDataFunc();
//       chartOptions.chart.type = chart.type;
//       chartOptions.series = [{ data: seriesData }];
//       chartOptions.xaxis.categories = categoriesData;
//       return (
//         <div key={index}>
//           <ReactApexChart
//             options={chartOptions}
//             series={chartOptions.series}
//             type={chart.type}
//             height={400}
//           />
//         </div>
//       );
//     });
//   };
//   return (
//     <div>
//       <div className='flex justify-center items-center'>
//         <input type="file" className={`py-4 px-6 font-poppins font-medium text-[18px] text-primary bg-blue rounded-[10px] outline-none`} onChange={handleFileUpload} />
//       </div>
//       {data && renderCharts()}
//     </div>
//   );
// };

// export default pgCharts;