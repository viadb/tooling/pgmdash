import React, { useState } from 'react';
import ReactApexChart from 'react-apexcharts';

const SecondCharts = () => {
  const [data, setData] = useState(null);

  const handleFileUpload = (event) => {
    const reader = new FileReader();
    reader.onload = (event) => {
      const fileContent = event.target.result;
      const jsonData = JSON.parse(fileContent);
      setData(jsonData);
    };
    reader.readAsText(event.target.files[0]);
  };

  const chartOptions = {
    chart: {
      type: 'bar',
    },
    series: [],
    xaxis: {
      categories: [],
    },
  };

  if (data) {
    chartOptions.series = data.series;
    chartOptions.xaxis.categories = data.categories;
  }

  return (
    <div>
      <div  className='flex justify-center items-center'>
        <input className={`py-4 px-6 font-poppins font-medium text-[18px] text-primary bg-blue rounded-[10px] outline-none`} type="file" onChange={handleFileUpload} />
      </div>
      {data && (
        <ReactApexChart options={chartOptions} series={chartOptions.series} type="bar" height={400} />
      )}
    </div>
  );
};

export default SecondCharts;