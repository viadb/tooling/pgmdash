import Navbar from "./Navbar";
import Footer from "./Footer";
import Hero from "./Hero";
import Charts from "./Charts";
import LineCharts from "./LineCharts";
import SecondCharts from "./SecondChart";
import PgCharts from "./PgCharts";


export {
  Navbar,
  Footer,
  Hero,
  Charts,
  LineCharts,
  SecondCharts,
  PgCharts,
};