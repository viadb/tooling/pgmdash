import { facebook, instagram, linkedin, twitter } from "../assets";

export const navLinks = [
  {
    id: "home",
    title: "Home",
  },
  {
    id: "options",
    title: "Options",
  },

];


export const footerLinks = [
  {
    title: "Title",
    links: [
      {
        name: "Link",
        link: "",
      },
      {
        name: "Link",
        link: "",
      },
      {
        name: "Link",
        link: "",
      },
      {
        name: "Link",
        link: "",
      },
      {
        name: "Link",
        link: "",
      },
    ],
  },
  {
    title: "Title ",
    links: [
      {
        name: "Link",
        link: "",
      },
      {
        name: "Link",
        link: "",
      },
      {
        name: "Link",
        link: "",
      },
      {
        name: "Link",
        link: "",
      },
    ],
  },
  {
    title: "Title",
    links: [
      {
        name: "Link",
        link: "",
      },
      {
        name: "Link",
        link: "",
      },
    ],
  },
];

export const socialMedia = [
  {
    id: "social-media-1",
    icon: instagram,
    link: "https://www.instagram.com/",
  },
  {
    id: "social-media-2",
    icon: facebook,
    link: "https://www.facebook.com/",
  },
  {
    id: "social-media-3",
    icon: twitter,
    link: "https://www.twitter.com/",
  },
  {
    id: "social-media-4",
    icon: linkedin,
    link: "https://www.linkedin.com/",
  },
];
